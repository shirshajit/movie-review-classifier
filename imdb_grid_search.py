"""
Tf-idf vectorization using the IMDb comments dataset
Different parameter combinations are tested using GridSearchCV and Tf-idf (Term Frequency- Inverse Document
Frequency) calculates words too common and removes them (assigns lower priority).
Very slow, processing the entire 50 000 comments will take about an hour, hence on 1000 comments are used
altogether (500 train and 500 test)
"""
from sklearn.model_selection import GridSearchCV
from sklearn.pipeline import Pipeline
from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction.text import TfidfVectorizer
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
import re
import pandas as pd


def preprocessor(text):                     # removes tags and moves emoticons to end of a text line using regex
    text = re.sub('<[^>]*>', '', text)
    emoticons = re.findall('(?::|;|=)(?:-)?(?:\)|\(|D|P)', text)
    text = re.sub('[\W]+', ' ', text.lower()) + ''.join(emoticons).replace('-', '')
    return text


def imdb_initializer():
    df = pd.read_csv('./movie_data.csv')
    df['review'] = df['review'].apply(preprocessor)
    # print(df.iloc[:100, :])
    # print(preprocessor(df.loc[0, 'review'][-50:]))
    # print(preprocessor('</a>This :) is :( a test :-)!'))
    # x = df[:, 'review']
    # y = df[:, 'sentiment']

    """Reducing dataset size to reduce processing time to 1/50th of original"""
    x_train = df.loc[:500, 'review'].values
    y_train = df.loc[:500, 'sentiment'].values
    x_test = df.loc[-500:, 'review'].values
    y_test = df.loc[-500:, 'sentiment'].values

    # x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.5, random_state=0)
    return x_train, x_test, y_train, y_test


def tokenizer_porter(text):                           # porter-stemmer algorithm for word stemming
    porter = PorterStemmer()
    return [porter.stem(word) for word in tokenizer(text)]


def tokenizer(text):                                   # splits text into tokens)
    return text.split()


stop = stopwords.words('english')
"""
print(tokenizer('runners like running and thus they run'))
print(tokenizer_porter('runners like running and thus they run'))
print([w for w in tokenizer_porter('a runner likes running and runs a lot')[-10:] if w not in stop])
"""

x_train, x_test, y_train, y_test = imdb_initializer()
tf_idf = TfidfVectorizer(strip_accents=None, lowercase=False, preprocessor=None)

param_grid = [{
    'vect__ngram_range': [(1, 1)],
    'vect__stop_words': [stop, None],
    'vect__tokenizer': [tokenizer, tokenizer_porter],
    'clf__penalty': ['l1', 'l2'],
    'clf__C': [1.0, 10.0, 100.0]
},
    {
        'vect__ngram_range': [(1, 1)],
        'vect__stop_words': [stop, None],
        'vect__tokenizer': [tokenizer, tokenizer_porter],
        'clf__penalty': ['l1', 'l2'],
        'clf__C': [1.0, 10.0, 100.0],
        'vect__use_idf': [False],
        'vect__norm':[None]
    }]

lr_tf_idf = Pipeline([('vect', tf_idf), ('clf', LogisticRegression(random_state=0, solver='liblinear'))])
gs_lr_tf_idf = GridSearchCV(lr_tf_idf, param_grid, scoring='accuracy', cv=5, verbose=1, n_jobs=-1)
gs_lr_tf_idf.fit(x_train, y_train)

print("Best parameter set: ", gs_lr_tf_idf.best_params_)
print("CV Accuracy: {0:.3f}".format(gs_lr_tf_idf.best_score_))
clf = gs_lr_tf_idf.best_estimator_
print("Test Accuracy: {0:.3f}".format(clf.score(x_test, y_test)))