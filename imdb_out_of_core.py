"""
Out-of-core learning takes a batch of files(reviews) at a time. The batch is then hashed using HashingVectorizer
and a linear classifier is used to separate positive and negative comments.
Much faster than tf-idf vectorization, but slightly less accurate
"""

import numpy as np
from nltk.corpus import stopwords
import re
from sklearn.feature_extraction.text import HashingVectorizer
from sklearn.linear_model import SGDClassifier
import pyprind
import pickle
import os

stop = stopwords.words('english')


def tokenizer   (text):              # tokenizes the text to form individual words
    text = re.sub('<[^>]*>', '', text)
    emoticons = re.findall('(?::|;|=)(?:-)?(?:\)|\(|D|P)', text)
    text = re.sub('[\W]+', ' ', text.lower()) + ''.join(emoticons).replace('-', '')
    tokenized = [w for w in text.split() if w not in stop]
    return tokenized


def stream_docs(path):                  # generator object(csv file) creation
    with open(path, 'r', encoding='utf-8') as csv:
        next(csv)       # header
        for line in csv:
            text, label = line[:-3], int(line[-2])
            yield text, label


def get_mini_batch(doc_stream, size):       # takes batch of 'size' number of files
    docs, y = [], []
    try:
        for _ in range(size):
            text, label = next(doc_stream)
            docs.append(text)
            y.append(label)
    except StopIteration:
        return None, None
    return docs, y


vect = HashingVectorizer(decode_error='ignore', n_features=2**21, preprocessor=None, tokenizer=tokenizer)
clf = SGDClassifier(loss='log', random_state=1, max_iter=1)
doc_stream = stream_docs(path='./movie_data.csv')
# print(next(stream_docs(path='./movie_data.csv')))

p_bar = pyprind.ProgBar(45)               # progress bar with 45 increments
classes = np.array([0, 1])
for _ in range(45):
    x_train, y_train = get_mini_batch(doc_stream, size=1000)
    if not x_train:
        break
    x_train = vect.transform(x_train)
    clf.partial_fit(x_train, y_train, classes=classes)
    p_bar.update()

x_test, y_test = get_mini_batch(doc_stream, size=5000)
x_test = vect.transform(x_test)
print("Accuracy {0:.3f}".format(clf.score(x_test, y_test)))
clf.partial_fit(x_test, y_test)

"""Saves the data into a binary pkl file"""
destination = os.path.join('movieclassifier', 'pkl_objects')
if not os.path.exists(destination):
    os.makedirs(destination)

pickle.dump(stop, open(os.path.join(destination, 'stopwords.pkl'), 'wb'), protocol=4)
pickle.dump(clf, open(os.path.join(destination, 'classifier.pkl'), 'wb'), protocol=4)
