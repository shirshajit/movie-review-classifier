"""Creates an SQL database for new movie reviews"""
import sqlite3
import os

conn = sqlite3.connect('reviews.sqlite')
c = conn.cursor()
c.execute("CREATE TABLE review_db" "(review TEXT, sentiment INTEGER, date TEXT)")  # cannot be run twice

example_1 = "I love this movie"
example_2 = 'I disliked this movie'
for example, val in zip([example_1, example_2], [1, 0]):
    c.execute("INSERT INTO review_db" "(review, sentiment, date) VALUES"
              " (?, ?, DATETIME('now'))", (example, val))
conn.commit()
conn.close()
# running the code again will cause more lines to be added
