"""
Unpickle-ing of the classifier.pkl and stop_words.pkl file is done here and the resulting data is used to build
the classifier model that makes the predictions
"""
from sklearn.feature_extraction.text import HashingVectorizer
import re
import pickle
import os

cur_dir = os.path.dirname(__file__)
stop = pickle.load(open(os.path.join(cur_dir, 'pkl_objects', 'stopwords.pkl'), 'rb'))


def tokenizer(text):              # tokenizes the text to form individual words
    text = re.sub('<[^>]*>', '', text)
    emoticons = re.findall('(?::|;|=)(?:-)?(?:\)|\(|D|P)', text)
    text = re.sub('[\W]+', ' ', text.lower()) + ''.join(emoticons).replace('-', '')
    tokenized = [w for w in text.split() if w not in stop]
    return tokenized


vect = HashingVectorizer(decode_error='ignore', n_features=2**21, preprocessor=None, tokenizer=tokenizer)
