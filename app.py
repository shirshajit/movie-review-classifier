"""
Using the Flask micro-environment to make an app that uses the saved binary data in the pkl file to make
predictions on user reviews determining whether they are positive or negative.
"""
from flask import Flask, render_template, request
from wtforms import Form, TextAreaField, validators
import pickle
import sqlite3
import os
import numpy as np

# Imports from movieclassifier directory
from vectorizer import vect
from update import update_model

app = Flask(__name__)

# Preparing the classifier
cur_dir = os.path.dirname(__file__)
clf = pickle.load(open(os.path.join(cur_dir, 'pkl_objects/classifier.pkl'), 'rb'))
db = os.path.join(cur_dir, 'reviews.sqlite')


# makes the prediction
def classify(document):
    label = {0: 'negative', 1: 'positive'}
    x = vect.transform([document])
    y = clf.predict(x)[0]
    probability = np.max(clf.predict_proba(x))
    return label[y], probability


# inserts the predicted user data into the classifier model
def train(document, y):
    x = vect.transform([document])
    clf.partial_fit(x, [y])


# enters values into the database
def sqlite_entry(path, doc, y):
    conn = sqlite3.connect(path)
    c = conn.cursor()
    c.execute("INSERT INTO review_db (review, sentiment, date)" 
              "VALUES (?, ?, DATETIME('now'))", (doc, y))
    conn.commit()
    conn.close()


# form class
class ReviewForm(Form):
    movie_review = TextAreaField('', [validators.DataRequired(), validators.length(min=15)])
    

# shows the form
@app.route('/')
def index():
    form = ReviewForm(request.form)
    return render_template('review_form.html', form=form)


# function called after review written, makes the prediction
@app.route('/results', methods=['POST'])
def results():
    form = ReviewForm(request.form)
    if request.method == 'POST' and form.validate():
        review = request.form['movie_review']
        y, probability = classify(review)
        return render_template('results.html', content=review, prediction=y, probability=round(probability*100, 2))
    return render_template('review_form.html', form=form)


# function that is called after the thanks page is displayed
@app.route('/thanks', methods=['POST'])
def feedback():
    feedback = request.form['feedback_button']
    review = request.form['review']
    prediction = request.form['prediction']

    inv_label = {'negative': 0,'positive': 1}
    y = inv_label[prediction]
    if feedback == 'Incorrect':
        y = int(not y)
    train(review, y)
    sqlite_entry(db, review, y)
    return render_template('thanks.html')


# run app
if __name__ == '__main__':
    app.run(debug=True)
    update_model(db_path=db, model=clf)
