"""
Creates the csv file for the IMDb dataset.
"""
import pyprind
import pandas as pd
import os
import numpy as np


def _create_csv():              # creates the csv file from the downloaded IMDb data
    p_bar = pyprind.ProgBar(50000)
    labels = {'pos': 1, 'neg': 0}
    df = pd.DataFrame()
    for folder in ('test', 'train'):
        for val in ('pos', 'neg'):
            path = './aclImdb/{0}/{1}'.format(folder, val)
            for file in os.listdir(path):
                with open(os.path.join(path, file), 'r', encoding='utf-8') as infile:
                    txt = infile.read()
                df = df.append([[txt, labels[val]]], ignore_index=True)
                p_bar.update()
    df.columns = ['review', 'sentiment']
    np.random.seed(0)
    df = df.reindex(np.random.permutation(df.index))
    # print(df)
    df.to_csv('./movie_data.csv')





